<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    echo "set value with redis (foo =>bar) <br>";

    Redis::set("foo","bar");
    echo "get foo <br>";
    return Redis::get('foo');
    
});

Route::get('/cache', function () {
    echo "=============================<br>";
    echo "set value with Cache (foo =>bar) <br>";
    Cache::store('redis')->put('foo', 'bar', 600); // 10 Minutes

    echo "get foo <br>";
    $value = Cache::get('foo');

    return $value;
    
});

//queue example with redis driver
Route::get('test', 'MailController@index');


//https://laravel.com/docs/8.x/session

Route::get('/session', function () {
    echo "=============================<br>";
    echo "set key with session (key =>some data) <br>";
    // Store a piece of data in the session...
    session(['key' => 'some data']);
    // Retrieve a piece of data from the session...
    $value = session('key');

    // Specifying a default value...
    $value = session('key', 'default');
    echo "get key <br>";
    return $value;
    
});
