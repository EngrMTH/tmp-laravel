-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2021 at 07:11 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(2, 'default', '{\"uuid\":\"4b3912f3-53c6-4010-90bd-c8ce902f90bd\",\"displayName\":\"App\\\\Jobs\\\\SendOrderEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendOrderEmail\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\SendOrderEmail\\\":9:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:44;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1617089854, 1617089854),
(3, 'default', '{\"uuid\":\"60fb0829-00c6-499f-a035-d00fc66f7ef4\",\"displayName\":\"App\\\\Jobs\\\\SendOrderEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\SendOrderEmail\",\"command\":\"O:23:\\\"App\\\\Jobs\\\\SendOrderEmail\\\":9:{s:5:\\\"order\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:9:\\\"App\\\\Order\\\";s:2:\\\"id\\\";i:26;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1617089923, 1617089923);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2021_03_30_064633_create_orders_table', 1),
(4, '2021_03_30_073110_create_jobs_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_count` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `item_count`, `created_at`, `updated_at`) VALUES
(1, 'Colin Morissette I', 7, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(2, 'Timmothy Stiedemann II', 5, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(3, 'Gregg Stokes', 7, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(4, 'Prof. Luella McGlynn', 2, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(5, 'Newell Smith', 4, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(6, 'Marianne Lesch Sr.', 8, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(7, 'Dallas Romaguera MD', 1, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(8, 'Hayley Heidenreich', 2, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(9, 'Kailyn Sawayn', 10, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(10, 'Dr. Ted Heaney PhD', 10, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(11, 'Moses Kassulke', 1, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(12, 'Thomas Mayer', 10, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(13, 'Mrs. Ernestine Bergnaum', 9, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(14, 'Mrs. Catalina McCullough', 8, '2021-03-30 03:57:01', '2021-03-30 03:57:01'),
(15, 'Dr. Leonora Hackett', 6, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(16, 'Alexys Collier', 7, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(17, 'Dr. Hanna Gulgowski', 7, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(18, 'Everardo Schumm I', 6, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(19, 'Reva Hickle', 3, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(20, 'Kali White DVM', 8, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(21, 'Dr. Darryl Collier', 6, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(22, 'Conner Harber Sr.', 10, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(23, 'Mr. Ethan Howell', 7, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(24, 'Domenica Flatley PhD', 5, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(25, 'Lawrence Nikolaus', 10, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(26, 'Mathilde Parker', 4, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(27, 'Carol Bauch', 6, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(28, 'Albert Hamill', 7, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(29, 'Mr. Rudolph Willms', 9, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(30, 'Miss Kendra Spinka DDS', 10, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(31, 'Prince Heidenreich', 1, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(32, 'Candace Beier', 2, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(33, 'Dr. Nathan Padberg', 1, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(34, 'Jennyfer Gutkowski', 10, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(35, 'Loren Raynor V', 3, '2021-03-30 03:57:02', '2021-03-30 03:57:02'),
(36, 'Madilyn Lynch DVM', 4, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(37, 'Mathew Schowalter', 6, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(38, 'Tressie Baumbach', 10, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(39, 'Mr. Max Flatley DVM', 10, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(40, 'Prof. Omer Aufderhar IV', 10, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(41, 'Crawford Schmeler', 10, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(42, 'Colleen Monahan', 4, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(43, 'Mr. Elliott Mayert MD', 8, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(44, 'Lempi Boyer', 3, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(45, 'Leora McCullough', 7, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(46, 'Miss Vicky Will II', 8, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(47, 'Michale Parker', 9, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(48, 'Bertrand Zboncak', 9, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(49, 'Avis Roberts I', 4, '2021-03-30 03:57:03', '2021-03-30 03:57:03'),
(50, 'Jackie Kunde MD', 5, '2021-03-30 03:57:03', '2021-03-30 03:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
